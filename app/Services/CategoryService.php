<?php

namespace App\Services;

class CategoryService
{
    public static function getCategories()
    {
        //$url = 'http://10.20.3.7:5000/services';
        $url = env('SERVICE_URL');
        try {
            $data = json_decode(file_get_contents($url), true);

        } catch (\Exception $e) {
            $data = "wrong";
        }
        return $data;
    }

    public static function getServices($category)
    {
        //$url = 'http://10.20.3.7:5000/services/' . $category;
        $url = env('SERVICE_URL') . '/' . $category;
        try {
            $data = json_decode(file_get_contents($url), true);

        } catch (\Exception $e) {
            $data = "wrong";
        }
        return $data;
    }
}
<?php

namespace App\Services;

use GuzzleHttp\Client;

class BranchService
{
    /**
     * @param $id
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function sendBranch($userId, $branchId)
    {
        $client = new Client();
        $res = $client->request('POST', 'http://10.20.3.7:5000/choose', [
            'json' => ['user_id' => $userId, 'branch_id' => $branchId]
        ]);

        return json_decode($res->getBody(), true);
    }
}
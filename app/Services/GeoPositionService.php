<?php

namespace App\Services;

use GuzzleHttp\Client;

class GeoPositionService
{
    /**
     * @param $latitude
     * @param $longtitude
     * @param $transport
     * @param $serviceId
     * @param $userId
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function sendUserRequest($userId, $serviceId, $latitude, $longtitude, $phoneNumber)
    {
        $options = [
            'user_id' => $userId,
            'service_id' => $serviceId,
            'lat' => $latitude,
            'lon' => $longtitude,
            'phone_number' => $phoneNumber
        ];

        $client = new Client();
        $res = $client->request('POST', 'http://10.20.3.7:5000/fetch', [
            'json' => $options
        ]);

        return json_decode($res->getBody(), true);
    }
}
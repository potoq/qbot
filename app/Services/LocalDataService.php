<?php


namespace App\Services;


use GuzzleHttp\Client;

class LocalDataService
{
    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getLat()
    {
        //$ans = json_decode($message, true);
        $m = LocalDataService::getJs();
        //return $m["message"]["location"]["latitude"];
        return $m["update_id"];
    }

    public static function getLLat($val)
    {
        return "55.815483";
    }

    public static function getLLon($val)
    {
        return "37.513376";
    }
    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getLong()
    {
        //$ans = json_decode($message, true);
        $m = LocalDataService::getJs();
        return $m["message"]["location"]["longitude"];
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function getJs()
    {
        /*$client = new Client();
        $res = $client->request('GET', 'http://10.20.3.7:5000/test', [
            'json' => ['key' => 'value']
        ]);*/


        $url = 'http://10.20.3.7:5000/test';

        $client = new Client();
        $res = $client->request('GET', $url, [
            'json' => ['key' => 'value']
        ]);

        $a = json_decode($res->getBody(), true);
        return $a["message"]["location"]["longitude"];

        //return $a;
    }
}
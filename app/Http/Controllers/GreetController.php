<?php

namespace App\Http\Controllers;

use App\Conversations\GreetConversation;
use App\Conversations\HelpConversation;
use App\Conversations\ServicesConversation;
use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;

class GreetController extends Controller
{
    /**
     * Place your BotMan logic here.
     */
    public function handle()
    {
        $botman = app('botman');

        $botman->listen();
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function greetConversation(BotMan $bot)
    {
        $userFromStartButton = $bot->getMessage()->getText() === '/start' ? true : false;
        $bot->startConversation(new GreetConversation($userFromStartButton));
        /*$userId = $bot->getUser()->getId();
        $bot->types($userId);*/

        $bot->startConversation(new ServicesConversation());
    }

    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function helpConversation(BotMan $bot)
    {
        $bot->startConversation(new HelpConversation());
    }
}

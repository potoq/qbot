<?php

namespace App\Http\Controllers;

use App\Conversations\OfficeConversation;
use App\Services\GeoPositionService;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Attachments\Location;
use Illuminate\Http\Request;

class OfficeController extends Controller
{

    protected $geoPosition;

    /**
     * Loaded through routes/botman.php
     * @param BotMan $bot
     * @param Location $location
     */
    public function getCoordinatesFromApi(BotMan $bot, Location $location)
    {
        $lat = $location->getLatitude();
        $lng = $location->getLongitude();

        $this->geoPosition = new GeoPositionService($lat, $lng);
    }

    /**
     * @return mixed
     */
    public function showGeoPosition()
    {
        return $this->geoPosition->toString;
    }

    /**
     * @param BotMan $bot
     */
    public function queueConversation(BotMan $bot)
    {
        $bot->startConversation(new OfficeConversation());
    }
}

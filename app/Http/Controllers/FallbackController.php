<?php

namespace App\Http\Controllers;

use App\Conversations\FallbackConversation;
use BotMan\BotMan\BotMan;
use Illuminate\Http\Request;

class FallbackController extends Controller
{
    /**
     * Loaded through routes/botman.php
     * @param  BotMan $bot
     */
    public function fallbackConversation(BotMan $bot)
    {
        $bot->startConversation(new FallbackConversation());
    }
}

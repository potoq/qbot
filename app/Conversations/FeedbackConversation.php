<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class FeedbackConversation extends Conversation
{
    public function askFeedback()
    {
        $question = Question::create("Пожалуйста, оцените уровень обслуживания")
            ->callbackId('ask_feed')
            ->addButtons([
                Button::create('1')->value('one'),
                Button::create('2')->value('two'),
                Button::create('3')->value('three'),
                Button::create('4')->value('four'),
                Button::create('5')->value('five'),
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->askForComplaint();
            }
        });
    }

    public function askForComplaint()
    {
        $question = Question::create("Вы можете написать предложение или жалобу! Или нажмите \"Отмена\"")
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_сomplaint')
            ->addButtons([
                Button::create('Все понравилось')
                    ->value('good'),
                Button::create('Отмена')
                    ->value('cancel')
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'path') {
                    //$this->say('path');
                } elseif ($answer->getValue() === 'late') {
                    //$this->say('late');
                    $this->bot->startConversation(new LateConversation());
                } elseif ($answer->getValue() === 'cancel') {
                    //$this->say('cancel');
                    $this->bot->startConversation(new CancelServiceConversation());
                } elseif ($answer->getValue() === 'add') {
                    //$this->say('add');
                } elseif ($answer->getValue() === 'calendar') {
                    //$this->say('calendar');
                };
            }
        });
    }
    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askFeedback();
    }
}

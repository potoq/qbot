<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use Illuminate\Foundation\Inspiring;

class FallbackConversation extends Conversation
{
    public function wrongCommand()
    {
        $question = Question::create("Такой команды не существует. Вернитесь в начало.")
            ->callbackId('wrong_command')
            ->addButtons([
                Button::create('Начать заново')->value('starter'),
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->bot->startConversation(new GreetConversation(false));
            }
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->wrongCommand();
        //$this->bot->typesAndWaits(1);
        //$this->bot->reply('Такой команды не существует. Вернитесь в начало.');

        /*$question = ButtonTemplate::create('Here is how I can help you:')->addButtons([
            ElementButton::create('💌 Edit subscription')->type('postback')->payload('subscribe'),
            ElementButton::create('👉 Christoph\'s Blog')->url('https://christoph-rumpel.com/')
        ]);

        $this->bot->reply($question);*/
    }
}

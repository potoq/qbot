<?php

namespace App\Conversations;

use App\Services\BranchService;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class BranchConversation extends Conversation
{
    protected $branchAddresses;

    public function __construct($branchAddresses) {

        $this->branchAddresses = $branchAddresses;
    }

    public function chooseAddress()
    {
        $question = Question::create("Выберите отделение банка")
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_branch');

        foreach ($this->branchAddresses as $key => $branchAddress)
        {
            $question->addButton(Button::create($branchAddress["address"] . "\nНа время: " . $branchAddress["visit_time"] . " (через " .
                $branchAddress["diff_time"]  . " мин.)")->value($key));
        }

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                //$this->bot->reply($answer->getValue());
                $userId = $this->bot->getUser()->getId();
                $branch = BranchService::sendBranch($userId, $answer->getValue());
                $this->bot->startConversation(new TicketConversation($branch));

                //$this->chooseServiceName($answer->getValue());
            }
        }, ['parse_mode' => 'Markdown']);
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->chooseAddress();
    }
}

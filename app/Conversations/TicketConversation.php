<?php

namespace App\Conversations;

use App\Services\BranchService;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use BotMan\BotMan\Messages\Outgoing\Question;
use Redirect;

class TicketConversation extends Conversation
{
    protected $branch;

    public function __construct($branch) {

        $this->branch = $branch;
    }

    public function ticketOptions()
    {
        $text = "Номер вашего талона: " . $this->branch["ticket"] . ". Вам нужно прийти к " . $this->branch["visit_time"] . " к окну " .
            $this->branch["window"] . ". Адрес банка: " . $this->branch["address"] . ". " . $this->branch["path_type"] . ", время: " .
            $this->branch["path_time"] . ". +1 Уралкоин вы заработали! Обменять их можно в разделе Уралкоины";

        // . ". Посмотреть маршрут: " . "[" . $this->branch["path_link"] .
        //            "](" . $this->branch["path_link"] . ")"

        $attachment = new Image('http://77.244.213.126:3333/qr?text=' . $this->branch["ticket"], [
            'custom_payload' => true,
        ]);

        // Build message object
        $message = OutgoingMessage::create()
            ->withAttachment($attachment);

        $this->bot->reply($message);

        $question = Question::create($text)
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_option')
            ->addButtons([
                Button::create('Построить маршрут')
                    ->value('path'),
                Button::create('Я опаздываю')
                    ->value('late'),
                Button::create('Я не приду')
                    ->value('cancel'),
                Button::create('Добавить услугу')
                    ->value('add'),
                Button::create('Добавить напоминание в календарь')
                    ->value('calendar')
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'path') {
                    $this->sendPath();
                    //$this->say('path');
                } elseif ($answer->getValue() === 'late') {
                    //$this->say('late');
                    $this->bot->startConversation(new LateConversation());
                } elseif ($answer->getValue() === 'cancel') {
                    //$this->say('cancel');
                    $this->bot->startConversation(new CancelServiceConversation());
                } elseif ($answer->getValue() === 'add') {
                    //$this->say('add');
                } elseif ($answer->getValue() === 'calendar') {
                    //$this->say('calendar');
                };
            }
        });
    }

    public function sendPath()
    {
        /*$url = $this->branch["path_link"];
        $link = "<script>window.open($url)</script>";

        echo $link;*/
        //redirect($this->branch["path_link"]);
        return Redirect::to($this->branch["path_link"]);
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->ticketOptions();
    }
}

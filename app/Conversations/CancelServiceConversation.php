<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class CancelServiceConversation extends Conversation
{
    public function cancelOperation()
    {
        $question = Question::create("Очень жаль, возвращайтесь еще!")
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_option')
            ->addButton(Button::create('На главное меню')
                    ->value('to_main'));

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'to_main') {
                    $this->bot->startConversation(new GreetConversation(false));
                };
            }
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->cancelOperation();
    }
}

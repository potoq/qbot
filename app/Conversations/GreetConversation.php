<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;

class GreetConversation extends Conversation
{
    protected $userFromStartButton;

    public function __construct(bool $userFromStartButton) {

        $this->userFromStartButton = $userFromStartButton;
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->userFromStartButton ? $this->welcome() : $this->continueDialog();
    }

    private function welcome()
    {
        //$this->bot->typesAndWaits(.5);
        //$this->bot->reply('Здравствуйте! Спасибо, что воспользовались услугами нашего бота.');
        // "Здравствуйте, я Бот, чем могу быть полезен?"
        $this->bot->reply("Здравствуйте, я Бот");
        //$this->bot->reply('Здравствуйте, я Бот, чем могу быть полезен?');
        //$this->bot->startConversation(new ServicesConversation());


        //$this->help();
    }

    private function continueDialog()
    {
        $this->bot->startConversation(new ServicesConversation());
    }

    private function help()
    {
        $this->bot->startConversation(new HelpConversation());

        /*$this->bot->typesAndWaits(.5);
        $this->bot->reply('ask');
        $question = Question::create('Are you in?')
            ->addButtons([
                Button::create('Yes please')
                    ->value('yes'),
                Button::create('Nope')
                    ->value('no'),
            ]);*/
    }
}

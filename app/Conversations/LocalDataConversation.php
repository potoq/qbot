<?php

namespace App\Conversations;

use App\Services\GeoPositionService;
use App\Services\LocalDataService;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Telegram\TelegramDriver;
use BotMan\Drivers\VK\VkDriver;
use BotMan\Drivers\Web\WebDriver;

class LocalDataConversation extends Conversation
{
    protected $serviceName;
    protected $phoneNumber;
    protected $latitude;
    protected $longtitude;

    public function __construct($serviceName) {
        $this->serviceName = $serviceName;
    }

    /**
     *
     */
    public function askPhoneNumber()
    {
        $question = Question::create("Введите контактный номер телефона")
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_phone');

        return $this->ask($question, function (Answer $answer) {
            if ($answer->getText()) {
                $this->phoneNumber = $answer->getText();
                $this->askForUserLocation();
            }
        });
    }

    public function askForUserLocation()
    {
        $question = Question::create("Мне нужно ваше местоположение. Нажмите соответствующую кнопку в меню")
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_location');

        /*$this->bot->group(['driver' => [TelegramDriver::class, VkDriver::class]], function(BotMan $bot) use ($question) {
            return $this->ask($question, function (Answer $answer) {
                //$ans1 = json_decode($answer->getValue(), true);
                $ans = 0;
                $this->latitude = LocalDataService::getLLat($ans);
                $this->longtitude = LocalDataService::getLLon($ans);

                $this->askForTransport();
            });
        });*/

        $this->bot->group(['driver' => [WebDriver::class]], function(BotMan $bot) use ($question) {
            return $this->ask($question, function (Answer $answer) {
                $location = $answer->getValue();
                $l = explode(",", str_replace(' ', '', $location));
                $this->latitude = $l[0];
                $this->longtitude = $l[1];

                $this->askForTransport();
            });
        });

    }

    public function askForTransport()
    {
        $question = Question::create('Как собираетесь добираться до отделения?')
            ->addButtons([
                Button::create('На машине')
                    ->value('car'),
                Button::create('Пешком')
                    ->value('walk'),
                Button::create('Общественный транспорт')
                    ->value('common'),
                Button::create('Бесплатное такси!')
                    ->value('taxi'),
            ]);

        $this->ask($question, function (Answer $answer) {
            $userId = $this->bot->getUser()->getId();
            /*$this->bot->reply($userId);
            $this->bot->reply($this->serviceName);
            $this->bot->reply($this->phoneNumber);
            $this->bot->reply($this->latitude);
            $this->bot->reply($this->longtitude);
            $this->bot->reply($answer->getValue());*/

            $locationOptions = GeoPositionService::sendUserRequest($userId, $this->serviceName, $this->latitude, $this->longtitude, $this->phoneNumber);
            $this->bot->startConversation(new BranchConversation($locationOptions));
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askPhoneNumber();
    }
}

<?php

namespace App\Conversations;

use App\Services\CategoryService;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;
use Illuminate\Foundation\Inspiring;

class ServicesConversation extends Conversation
{
    /**
     * User choose needed service option.
     */
    public function chooseService()
    {
        //$this->bot->typesAndWaits(.5);

        $question = Question::create("Чем могу быть полезен?")
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_service')
            ->addButtons([
                Button::create('Услуги для физ. лиц')->value('individual'),
                Button::create('Услуги для юр. лиц')->value('entity'),
                Button::create('Помощь')->value('help'),
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                if ($answer->getValue() === 'individual') {
                    //$this->say('individual');
                    $this->chooseServiceCategory();
                } elseif ($answer->getValue() === 'entity') {
                    //$this->say('entity');
                    $this->chooseServiceCategory();
                } elseif ($answer->getValue() === 'help') {
                    $this->say('help');
                };
            }
        });
    }

    /**
     * User choose needed service category.
     */
    private function chooseServiceCategory()
    {
        $question = Question::create("Выберите категорию")
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_category');

        foreach (CategoryService::getCategories() as $key => $category)
        {
            $question->addButton(Button::create($category)->value($key));
        }

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                //$this->bot->reply($answer->getText());
                $this->chooseServiceName($answer->getValue());
            }
        });
    }

    /**
     * User choose needed service name.
     * @param $category
     * @return ServicesConversation
     */
    private function chooseServiceName($category)
    {
        $question = Question::create("Выберите услугу")
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_service');

        foreach (CategoryService::getServices($category) as $key => $service)
        {
            $question->addButton(Button::create($service)->value($key));
        }

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                //$this->bot->reply($answer->getText());
                $this->bot->startConversation(new LocalDataConversation($answer->getValue()));
            }
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->chooseService();
    }
}

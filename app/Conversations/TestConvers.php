<?php

namespace App\Conversations;

use App\Services\LocalDataService;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;

class TestConvers extends Conversation
{
    protected $ans;

    public function test()
    {
        $question = Question::create("Мне нужно ваше местоположение. Нажмите соответствующую кнопку в меню")
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_location');

        return $this->ask($question, function (Answer $answer) {
            //$this->bot->reply($answer->getValue() . "1");
            //$this->bot->reply($answer->getText() . "2");

            //$ans1 = $answer->getValue();

            //$ans = \App\Services\LocalDataService::getJs();
            //$this->bot->reply($ans);
            //$latitude = \App\Services\LocalDataService::getLat();
            //$longitude = \App\Services\LocalDataService::getLong();
            $l = LocalDataService::getJs();

            $this->bot->reply($l);
            //$this->bot->reply($latitude);

            //$this->bot->reply($longitude);
            $this->bot->reply("b");
        });
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->test();
    }
}

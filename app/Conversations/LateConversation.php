<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Outgoing\Question;

class LateConversation extends Conversation
{
    public function askHowLate()
    {
        $question = Question::create('На сколько минут вы опоздаете?')
            ->fallback('Не удалось выполнить операцию.')
            ->callbackId('ask_option')
            ->addButtons([
                Button::create('1-5')
                    ->value('first'),
                Button::create('6-10')
                    ->value('second'),
                Button::create('10-20')
                    ->value('third'),
                Button::create('Больше 20 минут')
                    ->value('forth'),
                Button::create('Не знаю')
                    ->value('fifth')
            ]);

        return $this->ask($question, function (Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                /*if ($answer->getValue() === 'first') {
                    //$this->say('first');

                } elseif ($answer->getValue() === 'second') {
                    //$this->say('second');
                } elseif ($answer->getValue() === 'third') {
                    //$this->say('third');
                } elseif ($answer->getValue() === 'forth') {
                    //$this->say('forth');
                } elseif ($answer->getValue() === 'fifth') {
                    //$this->say('fifth');
                };*/
                $this->waitFor();
            }
        });
    }

    public function waitFor()
    {
        $this->bot->reply('Хорошо, ждем вас!');
    }

    /**
     * Start the conversation.
     *
     * @return mixed
     */
    public function run()
    {
        $this->askHowLate();
    }
}

## Установка

1. Клонировать проект.
2. Перейдите в приложение папки с помощью cd.
3. Запустите composer install на вашем cmd или терминале.
4. Скопируйте .env.example файл в .env в корневую папку. Вы можете ввести copy .env.example .env, если используете командную строку Windows или cp .env.example .env, если используете терминал Ubuntu.
5. Откройте файл .env и токены мессенджеров.
6. Запустить php artisan key:generate.
7. Запустить php artisan serve.
8. Перейдите к localhost:8000.

## About BotMan Studio

While BotMan itself is framework agnostic, BotMan is also available as a bundle with the great [Laravel](https://laravel.com) PHP framework. This bundled version is called BotMan Studio and makes your chatbot development experience even better. By providing testing tools, an out of the box web driver implementation and additional tools like an enhanced CLI with driver installation, class generation and configuration support, it speeds up the development significantly.

You can find the BotMan and BotMan Studio documentation at [http://botman.io](http://botman.io).

## License

BotMan is free software distributed under the terms of the MIT license.


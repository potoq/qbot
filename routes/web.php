<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Services\BranchService;
use GuzzleHttp\Client;

Route::get('/', function () {
    return view('welcome');
});

Route::match(['get', 'post'], '/botman', 'BotManController@handle');
Route::get('/botman/tinker', 'BotManController@tinker');


Route::get('/botman/test', function () {

    $url = env('SERVICE_URL');
    $category = "additional_features";
    //$url = 'http://10.20.3.7:5000/services/' . $category;
    $url2 = env('SERVICE_URL') . '/' . $category;

    $url = 'http://10.20.3.7:5000/services';
    $url2 = 'http://10.20.3.7:5000/services/additional_features';
    $client = new Client();
    /*$res1 = $client->request('GET', $url, [
        'json' => ['eng' => 'rus']
    ]);
    $res2 = $client->request('GET', $url2, [
        'json' => ['eng' => 'rus']
    ]);*/

    /*$r1 = json_decode($res1->getBody(), true);
    $r2 = json_decode($res2->getBody(), true);
    dd($r1, $r2);*/

    $url3 = 'http://77.244.213.126:3333/';

    $options = [
        'text' => 'ffrefgtrg1232421515tgtrgrtgrt'
    ];

    $res = $client->request('POST', $url3, [
        'json' => $options
    ]);

    //$res->getBody()->
    dd($res, $res->getBody());
    /*$a = json_decode(file_get_contents($url2), true);

    $c = json_decode(file_get_contents($url), true);
    dd($c, $a);*/
    //dd($url, $url2);

    /*$client = new Client();
    $r = $client->request('POST', 'http://10.20.3.7:5000/fetch', [
        'json' => ['user_id' => '123']
    ]);*/

    /*$i = "55.781906, 37.614331";
    $string = str_replace(' ', '', $i);
    $ii = explode(",", str_replace(' ', '', $i));

    $options = [
        'user_id' => 123,
        'service_id' => 'service',
        'lat' => "55.75222",
        'lon' => "37.61556",
        'transport' => "car"
    ];

    $client = new Client();
    $res = $client->request('POST', 'http://10.20.3.7:5000/fetch', [
        'json' => $options
    ]);

    $res2 = json_decode($res->getBody(), true);
    $question = [];

    foreach ($res2 as $key => $branchAddress)
    {
        $question[$key] = ($branchAddress["address"] . "\nНа время: " . $branchAddress["visit_time"] . " (через " .
            $branchAddress["diff_time"]  . " мин.)");
        //$question[$key] = $branchAddress["address"];
    }*/

    //dd($res2, $question);

});

Route::get('/botman/test2', function () {
    $url = 'http://10.20.3.7:5000/test';

    $client = new Client();
    $res = $client->request('GET', $url, [
        'json' => ['key' => 'value']
    ]);

    $a = json_decode($res->getBody(), true);
    return $a["message"]["location"]["longitude"];
});
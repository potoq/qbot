<?php
use App\Http\Controllers\BotManController;
use App\Http\Controllers\FallbackController;
use App\Http\Controllers\GreetController;
use App\Http\Controllers\OfficeController;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\Drivers\Telegram\TelegramDriver;

$botman = resolve('botman');

//$botman->say("Привет, я Поток, официальный бот Уралсиб банка.\n\nЯ могу записать вас в очередь и ответить на ваши вопросы.\n\nНажмите СТАРТ, чтобы начать общение.\n\nЕсли я не смог вам помочь, позвоните по телефону +74957237777", '');

/*$botman->on('event', function($payload, $bot) {
    $bot->reply("Привет, я Поток, официальный бот Уралсиб банка.\n\nЯ могу записать вас в очередь и ответить на ваши вопросы.\n\nНажмите СТАРТ, чтобы начать общение.\n\nЕсли я не смог вам помочь, позвоните по телефону +74957237777");
});*/

// test reply
$botman->hears('Hi', function ($bot) {

    $bot->reply('Hello!');
});


    $botman->hears('/start', GreetController::class.'@greetConversation');
    $botman->hears('/support', function ($bot) {
        $bot->reply("Здравствуйте, я Иван. Какой у вас вопрос?");
    });
    $botman->fallback(FallbackController::class.'@fallbackConversation');

//$botman->hears('/start', GreetController::class.'@greetConversation');

/*$botman->group(['driver' => [TelegramDriver::class]], function(BotMan $bot) {
    $bot->hears('/start', GreetController::class.'@greetConversation');
    $bot->receivesLocation(OfficeController::class.'@getCoordinatesFromApi');
});*/

//$botman->hears('/help', GreetController::class.'@helpConversation');





/*$botman->hears('/img', function ($bot) {

    $attachment = new Image('https://upload.wikimedia.org/wikipedia/commons/8/8f/Qr-4.png', [
        'custom_payload' => true,
    ]);

    // Build message object
    $message = OutgoingMessage::create()
        ->withAttachment($attachment);

// Reply message object
    $bot->reply($message);
});*/
/*$botman->hears('ank', function ($bot) {

    $bot->startConversation(new \App\Conversations\TestConvers());

});*/